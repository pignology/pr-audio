/* $Id: praudio-s.c 3553 2011-05-05 06:14:19Z nanang $ */
/* 
 * PigRemote Modifications (C) 2013 Pignology, LLC
 * Copyright (C) 2008-2011 Teluu Inc. (http://www.teluu.com)
 * Copyright (C) 2003-2008 Benny Prijono <benny@prijono.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */

#include <pjsua-lib/pjsua.h>
#include <unistd.h>
#include <stdbool.h>

#define THIS_FILE   "PRAudio"
#define SIP_DOMAIN  "PigRemote"
#define SIP_USER    "PR-remote"
#define SIP_PASSWD  "notsosecret"
#define SIP_PORT        37372            /* Listening SIP port           */
#define RTP_PORT        37373        /* RTP port                        */

pjsua_acc_id acc_id;
pj_status_t status;

/* Display error and exit application */
static void error_exit(const char *title, pj_status_t status)
{
  pjsua_perror(THIS_FILE, title, status);
  pjsua_destroy();
  exit(1);
}


int initpjsua(bool isLocal, char *stunserver, int inputDeviceIndex, int outputDeviceIndex)
{     
    /* Create pjsua first! */
    status = pjsua_create();
    if (status != PJ_SUCCESS)
    {
        error_exit("Error in create()", status);
        return 1;
    }
     
    /* If argument is specified, it's got to be a valid SIP URL */
    /*if (argc > 1) {
     status = pjsua_verify_url(argv[1]);
     if (status != PJ_SUCCESS) error_exit("Invalid URL in argv", status);
     }*/
     
    /* Init pjsua */
    {
        printf("****************init pjsua");
        pjsua_config cfg;
        pjsua_logging_config log_cfg;
         
         
        pjsua_config_default(&cfg);
        //cfg.cb.on_incoming_call = &on_incoming_call;
        //cfg.cb.on_call_media_state = &on_call_media_state;
        //cfg.cb.on_call_state = &on_call_state;
        if (isLocal == 0)
        {
            //not local, set stun server
            cfg.stun_srv_cnt = 1;
            cfg.stun_srv[0] = pj_str(stunserver);
        }
        else
        {
            cfg.stun_srv_cnt = 0;
            cfg.stun_srv[0] = pj_str("stun.pjsip.org");
        }
    
        //PJSUA Media CFG
        pjsua_media_config media_cfg;
        pjsua_media_config_default(&media_cfg);
        media_cfg.no_vad = PJ_FALSE;
        media_cfg.enable_ice = PJ_FALSE;
        media_cfg.ec_tail_len = 100;
         
        pjsua_logging_config_default(&log_cfg);
        log_cfg.console_level = 5;
        //log_cfg.cb = &showLog;
         
        //status = pjsua_init(&cfg, &log_cfg, NULL);
        status = pjsua_init(&cfg, &log_cfg, &media_cfg);
        if (status != PJ_SUCCESS)
        {
            error_exit("Error in init()", status);
            return 1;
        }
    }
     
    /* Add UDP transport. */
    {
        printf("Adding transport.");
        printf("**************** Add UDP Transport\n");
        pjsua_transport_config cfg;
         
        pjsua_transport_config_default(&cfg);
        cfg.port = SIP_PORT;
        status = pjsua_transport_create(PJSIP_TRANSPORT_UDP, &cfg, NULL);
        if (status != PJ_SUCCESS)
        {
            error_exit("Error creating transport", status);
            return 1;
        }
    }
     
    /* Initialization is done, now start pjsua */
    printf("**************** Start PJSUA\n");
    status = pjsua_start();
    if (status != PJ_SUCCESS)
    {
        error_exit("Error starting voip", status);
        return 1;
    }
     
    /* Register to SIP server by creating SIP account. */
    {
        pjsua_acc_config cfg;
         
        pjsua_acc_config_default(&cfg);
        cfg.id = pj_str("sip:" SIP_USER "@" SIP_DOMAIN);
        cfg.reg_uri = pj_str("sip:" SIP_DOMAIN);
        cfg.cred_count = 1;
        cfg.cred_info[0].realm = pj_str(SIP_DOMAIN);
        cfg.cred_info[0].scheme = pj_str("digest");
        cfg.cred_info[0].username = pj_str(SIP_USER);
        cfg.cred_info[0].data_type = PJSIP_CRED_DATA_PLAIN_PASSWD;
        cfg.cred_info[0].data = pj_str(SIP_PASSWD);
        cfg.register_on_acc_add = 0;
         
        /* RTP Transport Config*/
        pjsua_transport_config  rtp_cfg;
        pjsua_transport_config_default(&rtp_cfg);
        rtp_cfg.port = RTP_PORT;
        cfg.rtp_cfg = rtp_cfg;
        
        status = pjsua_acc_add(&cfg, PJ_TRUE, &acc_id);
        if (status != PJ_SUCCESS)
        {
            error_exit("Error adding account", status);
            return 1;
        }
                 
        printf("PJSIP Init'd.\n");
    }
     
    printf("returning initpjsua\n");
    return 0;
}

/*
 * main()
 */
int main(int argc, char *argv[])
{

    printf("Getting audio devices...");
     
    int ret = initpjsua(true, "stun1.pignology.net", -1, -1);
     
    int dev_count;
    pjmedia_aud_dev_index dev_idx;
    pj_status_t status;
    dev_count = pjmedia_aud_dev_count();
    printf("\n\nGot %d audio devices\n", dev_count);
    for (dev_idx=0; dev_idx<dev_count; ++dev_idx) {
        pjmedia_aud_dev_info info;
        status = pjmedia_aud_dev_get_info(dev_idx, &info);
        printf("%d. %s (in=%d, out=%d)\n", dev_idx, info.name, info.input_count, info.output_count);
    }
     
    pjsua_call_hangup_all();
    pjsua_destroy();
    printf("Done getting audio devices.\n\n");

    return 0;
}
