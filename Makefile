all: pr-audio pjsip-listdev

pr-audio: pr-audio.c
	$(CC) -o $@ $< `pkg-config --cflags --libs libpjproject libconfig`

pjsip-listdev: pjsip-listdev.c
	$(CC) -o $@ $< `pkg-config --cflags --libs libpjproject`

clean:
	rm -f pr-audio.o pr-audio pjsip-listdev.o pjsip-listdev
